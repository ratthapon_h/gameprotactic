﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Script.AbilityNS;
using UnityEngine;
using Assets.Script.FieldNS;

namespace Assets.Script.CharacterNS
{
    public class Character 
    {
        public string name { get; set; }
        public GameObject gameObject; // represent game object
        public List<Ability> abilityList { get; set; }
        public int teamNo { get; set; } //  0=ally , 
        public int delayTurn { get; set; }
        private List<Condition> conditionList { get; set; }
        private ArrayList buffList { get; set; }
        private ArrayList types { get; set; } // for amplilfy damage
        public Attribute attribute { get; set; }

        public bool[,] movePattern { get; set; }
        public int moveRadius { get; set; }

        public Coordinate currentCoord { get; set; }

        // Use this for initialization
        public Character()
        {
            abilityList = new List<Ability>();
            abilityList.Add(new Attack());
            //teamNo = 0; // default
            conditionList = new List<Condition>();
            buffList = new ArrayList();
            types = new ArrayList();
            moveRadius = 7; // default
            currentCoord = new Coordinate();
            BuildMovePattern();
        }

        public Character(int teamNo, int moveRadius, Coordinate startCoord)
        {
            abilityList = new List<Ability>();
            abilityList.Add(new Attack());
            //teamNo = 0; // default
            conditionList = new List<Condition>();
            buffList = new ArrayList();
            types = new ArrayList();
            this.teamNo = teamNo;
            this.moveRadius = moveRadius;
            this.currentCoord = startCoord;
            if (teamNo == 0)
            {
                gameObject = GameObject.Instantiate(Resources.Load("test_Ally")) as GameObject;
                
            }
            else {
                gameObject = GameObject.Instantiate(Resources.Load("test_Enemy")) as GameObject;
            }
            gameObject.transform.position = startCoord.toVecter3();
            BuildMovePattern();
        }

        // Update is called once per frame

        protected void BuildMovePattern() {
            // square
            movePattern = new bool[moveRadius, moveRadius];
            for (int i = 0; i < moveRadius; i++)
            {
                for (int j = 0; j < moveRadius; j++)
                {
                    movePattern[i, j] = true;
                }
            }
        }

        public void MoveTo(Coordinate targetCoord)
        {
            this.gameObject.transform.position = Vector3.MoveTowards(currentCoord.toVecter3(), targetCoord.toVecter3(), Vector3.Distance(currentCoord.toVecter3(), targetCoord.toVecter3()));
            currentCoord = targetCoord;
        }

        public void CastAbility(Ability ability , Coordinate targetCoord, Character[] charactersInAoE, Field field) {
            ability.DoAction(this,targetCoord,charactersInAoE,field);
        }

        public void ProcessConditions(string currentPhase) {
            if(this.conditionList.Count>0){
                foreach(Condition con in conditionList){
                    con.DoEffect(this,currentPhase);
                }
            }
        }

        public void ProcessFieldEvent(List<FieldObject> filedObjects) { 
        
        }
    }
}