﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Script.CharacterNS
{
    public class Attribute
    {
        public int maxHP { get; set; }
        public int maxSP { get; set; }
        public int Atk { get; set; }
        public int currentHP { get; set; }
        public int currentSP { get; set; }

    }
}
