﻿using UnityEngine;
using System.Collections;
using Assets.Script.AbilityNS;
using Assets.Script.CharacterNS;

namespace Assets.Script.CharacterNS
{
    public class SparkingMan : Character
    {

        public SparkingMan()
        {
            teamNo = 1;
            attribute = new Attribute();
            attribute.Atk = 5;
            attribute.maxHP = 100;
            attribute.maxSP = 15;
            attribute.currentHP = attribute.maxHP;
            attribute.currentSP = attribute.maxSP;
        }
    }

}


