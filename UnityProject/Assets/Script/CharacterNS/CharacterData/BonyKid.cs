﻿using UnityEngine;
using System.Collections;
using Assets.Script.AbilityNS;
using Assets.Script.CharacterNS;

namespace Assets.Script.CharacterNS
{
    public class BonyKid : Character
    {

        public BonyKid()
        {
            teamNo = 0;
            abilityList.Add(new SkeletonImpact());
            abilityList.Add(new BoneBoomerang());
            abilityList.Add(new Frightening());
            attribute = new Attribute();
            attribute.Atk = 7;
            attribute.maxHP = 40;
            attribute.maxSP = 12;
            attribute.currentHP = attribute.maxHP;
            attribute.currentSP = attribute.maxSP;
        }

    }
}

