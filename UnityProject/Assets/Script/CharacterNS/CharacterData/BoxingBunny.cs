﻿using UnityEngine;
using System.Collections;
using Assets.Script.AbilityNS;
using Assets.Script.CharacterNS;

namespace Assets.Script.CharacterNS
{
    public class BoxingBunny : Character
    {
        public BoxingBunny()
        {
            teamNo = 0;
            abilityList.Add(new Pistol());
            abilityList.Add(new BunnyHop());
            attribute = new Attribute();
            attribute.Atk = 5;
            attribute.maxHP = 55;
            attribute.maxSP = 10;
            attribute.currentHP = attribute.maxHP;
            attribute.currentSP = attribute.maxSP;
        }
    }
}