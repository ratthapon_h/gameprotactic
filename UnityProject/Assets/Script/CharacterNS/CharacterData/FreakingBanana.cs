﻿using UnityEngine;
using System.Collections;
using Assets.Script.AbilityNS;
using Assets.Script.CharacterNS;

namespace Assets.Script.CharacterNS
{
    public class FreakingBanana : Character
    {
        public FreakingBanana()
        {
            teamNo = 0;
            abilityList.Add(new BananaHentai());
            attribute = new Attribute();
            attribute.Atk = 3;
            attribute.maxHP = 50;
            attribute.maxSP = 10;
            attribute.currentHP = attribute.maxHP;
            attribute.currentSP = attribute.maxSP;
        }
    }
}
