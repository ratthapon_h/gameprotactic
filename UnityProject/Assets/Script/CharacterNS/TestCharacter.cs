﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Script.CharacterNS
{
    public class TestCharacter : Character
    {
        public TestCharacter() : base(){
        
        }
        protected void BuildMovePattern()
        {
            // square
            movePattern = new bool[moveRadius, moveRadius];
            for (int i = 0; i < moveRadius; i++)
            {
                for (int j = 0; j < moveRadius; j++)
                {
                    movePattern[i, j] = true;
                }
            }
        }
    }
}
