﻿using UnityEngine;
using System.Collections;
using Assets.Script.AbilityNS;
using Assets.Script.CharacterNS;
using Assets.Script.FieldNS;
using System.Collections.Generic;
using System.IO;

public class Field
{
    public int[,] moveAbleCoords { get; set; } // 1 moveable 0 unmoveable, 2 air only  X,Y coord Col,Row
    // currently unused eventsObjects & characters
    public List<FieldObject> eventsObjects { get; set; }
    public List<Character> characters { get; set; } //  row col team{0 allies 1 enemey 2 natural (field object)}team{0 allies 1 enemey}
    public int nCol { get; set; }
    public int nRow { get; set; }
    public Field()
    {
        this.initiateMoveable();
        eventsObjects = new List<FieldObject>();
        characters = new List<Character>();
        // init value should store in text file
    }

    public void initiateFormation(Formation formation)
    {
        // load formation setting 
        characters.AddRange(formation.characters);
    }

    public void initiateFormation(Formation allyFormation, Formation enemyFormation)
    {
        // load formation setting 
        characters.AddRange(allyFormation.characters);
        characters.AddRange(enemyFormation.characters);
    }

    private void initiateMoveable()
    {
        string filePath = Application.dataPath+"/Resources/fieldMovablePattern.txt";
        string[] datas = File.ReadAllLines(filePath);
        Debug.Log("Load map " + filePath);
        this.nRow = datas.Length;
        this.nCol = datas[0].Length;
        Debug.Log("map size " + nRow + " " + nCol);
        moveAbleCoords = new int[this.nCol, this.nRow];
        for (int i = 0; i < nRow; i++)
        {
            for (int j = 0; j < nCol; j++)
            {
                moveAbleCoords[j, i] = int.Parse(datas[i][j].ToString());
            }
        }
        
    }
    private void DrawMovableTile(Character selectedCharacter)
    {
        Coordinate[] tilesCoords = this.ValidCoords(selectedCharacter.currentCoord, selectedCharacter.movePattern);
        for (int i = 0; i < tilesCoords.Length; i++)
        {
            Vector3 tilePoint = tilesCoords[i].toVecter3();
            MonoBehaviour.Instantiate(Resources.Load("MoveableTile"), tilePoint, Quaternion.identity);
        }
    }

    public Coordinate[] ValidCoords(Coordinate centroit, bool[,] pattern)
    {
        int nCol = pattern.GetLength(0);
        int nRow = pattern.GetLength(1);
        List<Coordinate> validCoords = new List<Coordinate>();
        for (int i = 0; i < nCol; i++)
        {
            if (centroit.X - nCol / 2 + i < 0 || centroit.X - nCol / 2 + i >= this.nCol)
            {
                continue;
            }
            for (int j = 0; j < nRow; j++)
            {
                if (centroit.Y - nRow / 2 + j < 0 || centroit.Y - nRow / 2 + j >= this.nRow)
                {
                    continue;
                }
                Coordinate tileCoord = new Coordinate(centroit.X - nCol / 2 + i, centroit.Y - nRow / 2 + j);
                if (pattern[i, j] == true && this.moveAbleCoords[tileCoord.X, tileCoord.Y] > 0)
                {
                    validCoords.Add(tileCoord);
                }
            }
        }
        return validCoords.ToArray();
    }

    private void SetTargetableCharacter(Character selectedCharacter, Ability ability)
    {
        Coordinate[] tilesCoords = this.ValidCoords(selectedCharacter.currentCoord, ability.castPattern);
        for (int i = 0; i < characters.Count; i++)
        {
            for (int j = 0; j < tilesCoords.Length; j++)
            {
                Character tempChar = characters[i];
                if ((tempChar.teamNo == ability.target || ability.target == -1) && tempChar.currentCoord == tilesCoords[j])
                {
                    characters[i].gameObject.tag = "targetablecharacter";
                }
            }
        }
    }

    private void DrawTargetableTile(Coordinate currentCoord, bool[,] pattern)
    {
        Coordinate[] tilesCoords = this.ValidCoords(currentCoord, pattern);
        for (int i = 0; i < tilesCoords.Length; i++)
        {
            Vector3 tilePoint = tilesCoords[i].toVecter3();
            MonoBehaviour.Instantiate(Resources.Load("TargetableTile"), tilePoint, Quaternion.identity);
        }
    }

    public void ApplyTargetable(Character selectedCharacter, Ability ability)
    {
        SetTargetableCharacter(selectedCharacter, ability);

        DrawTargetableTile(selectedCharacter.currentCoord, ability.castPattern);
    }

    public void ApplyMoveable(Character selectedCharacter)
    {
        DrawMovableTile(selectedCharacter);
    }

    public void ApplyAoE(Coordinate casterPosition, Coordinate centroit, Ability ability)
    {
        bool[,] flipAoE = new bool[ability.AoE.GetLength(0), ability.AoE.GetLength(1)];
        bool[,] transposAoE = new bool[ability.AoE.GetLength(0), ability.AoE.GetLength(1)];
        for (int i = 0; i < flipAoE.GetLength(0); i++)
        {
            for (int j = 0; j < flipAoE.GetLength(1); j++)
            {
                flipAoE[i, j] = ability.AoE[i, j];
                transposAoE[i, j] = flipAoE[i, j];
            }
        }
        if (Mathf.Abs(centroit.Y - casterPosition.Y) >= Mathf.Abs(centroit.X - casterPosition.X))
        {  // diff y > dif x


            bool flipByX = (centroit.Y - casterPosition.Y) < 0; // top vs bottom
            if (flipByX)
            {
                for (int i = 0; i < flipAoE.GetLength(0); i++)
                {
                    for (int j = 0; j < flipAoE.GetLength(1); j++)
                    {
                        flipAoE[i, j] = ability.AoE[i, flipAoE.GetLength(1) - j - 1];
                        transposAoE[i, j] = flipAoE[i, j];
                    }
                }
            }
        }
        else
        {
            if ((centroit.X - casterPosition.X) <= 0)
            {
                for (int i = 0; i < flipAoE.GetLength(0); i++)
                {
                    for (int j = 0; j < flipAoE.GetLength(1); j++)
                    {
                        transposAoE[i, j] = flipAoE[flipAoE.GetLength(1) - j - 1, flipAoE.GetLength(0) - i - 1];
                    }
                }
            }
            if ((centroit.X - casterPosition.X) > 0)
            {
                for (int i = 0; i < flipAoE.GetLength(0); i++)
                {
                    for (int j = 0; j < flipAoE.GetLength(1); j++)
                    {
                        transposAoE[i, j] = flipAoE[j, i];
                    }
                }
            }
        }
        DrawAoETile(centroit, transposAoE);
    }

    private void DrawAoETile(Coordinate centroit, bool[,] pattern)
    {
        Coordinate[] tilesCoords = this.ValidCoords(centroit, pattern);
        for (int i = 0; i < tilesCoords.Length; i++)
        {
            Vector3 tilePoint = tilesCoords[i].toVecter3();
            MonoBehaviour.Instantiate(Resources.Load("AoETile"), tilePoint, Quaternion.identity);
        }
    }

    public Character[] GetCharactersInAoE(Coordinate casterPosition, Coordinate centroit, Ability ability)
    {
        List<Character> charactersInAoEList = new List<Character>();
        Coordinate[] tilesCoords = this.ValidCoords(centroit, ability.AoE);
        for (int i = 0; i < characters.Count; i++)
        {
            for (int j = 0; j < tilesCoords.Length; j++)
            {
                Character tempChar = characters[i];
                if ((tempChar.teamNo == ability.target || ability.target == -1) && tempChar.currentCoord == tilesCoords[j])
                {
                    charactersInAoEList.Add(tempChar);
                }
            }
        }
        return charactersInAoEList.ToArray();
    }

    public Character CharacterAtCoord(Coordinate coord)
    {
        for (int i = 0; i < characters.Count; i++)
        {
            if (characters[i].currentCoord == coord)
            {
                Debug.Log("Char at " + coord.X + " " + coord.Y);
                return characters[i];
            }
        }
        return null;
    }

    public void DoEventAtTile(Character actor, Coordinate tileCoord, string trigger)
    {
        foreach (FieldObject fieldObject in this.eventsObjects)
        {

            if (trigger.Equals("moveon") && fieldObject.isMoveTrigger)
            {
                Debug.Log("Equal coord " + (fieldObject.coord == actor.currentCoord));
                if (fieldObject.coord == actor.currentCoord)
                {
                    fieldObject.ApplyEffectBy(actor);
                }
            }
            if (trigger.Equals("attacked") && fieldObject.isAttackedTrigger)
            {
                fieldObject.ApplyEffectBy(actor);
            }
            if (trigger.Equals("applycondition") && fieldObject.isConditionTrigger)
            {
                fieldObject.ApplyEffectBy(actor);
            }

        }
        this.UpdateFieldObjects();
    }

    public void ClearTile()
    {
        this.ClearTile("moveabletile");
        this.ClearTile("targetabletile");
        this.ClearTile("aoetile");
    }
    public void ClearTile(string tileNameTag)
    {
        foreach (GameObject tile in GameObject.FindGameObjectsWithTag(tileNameTag))
        {
            MonoBehaviour.Destroy(tile);
        }
    }

    public void UpdateFieldObjects()
    {
        eventsObjects.RemoveAll(x => x.lifeTime == 0);
    }
    public void UpdateFieldObjects(int lifeTime)
    {
        foreach (FieldObject fieldObject in this.eventsObjects)
        {
            fieldObject.lifeTime = fieldObject.lifeTime - lifeTime;
        }
    }

}
