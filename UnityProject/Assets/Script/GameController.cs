﻿using UnityEngine;
using System.Collections;
using Assets.Script.AbilityNS;
using Assets.Script.CharacterNS;

public class GameController : MonoBehaviour
{
    public string phase { get; set; }
    public Character selectedCharacter { get; set; }
    private Coordinate tempCharacterCoord;
    private Coordinate tempAoECoord;
    private Ability selectedAbility;
    private Field field {  get;  set; }
    // Use this for initialization
    void Start()
    {
        field = new Field();
        field.initiateFormation(new Formation());
        tempAoECoord = new Coordinate();
        selectedCharacter = field.characters[0];
        phase = "standby";
    }

    // Update is called once per frame
    void Update()
    {

        switch (phase)
        {
            default:
            case "standby":
                PhaseStandby();
                break;
            case "select":  // why we should have select phase?
                PhaseSelect();
                break;
            case "move":
                PhaseMove();
                break;
            case "pickability":
                PhasePickAbility();
                break;
            case "targeting":
                PhaseTargetting();
                break;
            case "confirmtarget":
                PhaseConfirmTarget();
                break;
            case "action":
                phase = "select";
                break;

        }
    }

    void PhaseStandby() { 
        // uuse for update condition and regen mana
        //field.characters.Sort((x, y) => x.delayTurn - y.delayTurn);
        foreach(Character c in field.characters){
            c.ProcessConditions(this.phase);
        }
        field.UpdateFieldObjects(1);
        phase = "select";
    }

    void PhaseSelect()
    {
        field.characters.Add(selectedCharacter);
        field.characters.RemoveAt(0);
        selectedCharacter = field.characters[0];
        if (selectedCharacter.delayTurn > 0)
        {
            selectedCharacter.delayTurn -= 1;
            phase = "standby";
        }
        else {
            field.ClearTile();
            field.ApplyMoveable(selectedCharacter);
            tempCharacterCoord = selectedCharacter.currentCoord;
            phase = "move"; // go to neext phase
        }
        if (Input.GetMouseButtonDown(0) && false)
        {
            RaycastHit hitInfo = new RaycastHit();
            bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
            if (hit)
            {
                if (hitInfo.collider.gameObject.tag == "character")
                {
                    Debug.Log("Select " + hitInfo.collider.gameObject.name);
                    selectedCharacter = field.CharacterAtCoord(Coordinate.fromVecter3(hitInfo.collider.gameObject.transform.position));
                    field.ClearTile();
                    Debug.Log("Game object "+selectedCharacter.currentCoord.X + " "+selectedCharacter.movePattern.GetLength(0));
                    field.ApplyMoveable(selectedCharacter);
                    tempCharacterCoord = selectedCharacter.currentCoord;
                    phase = "move"; // go to neext phase
                }
            }
        }
    }
    void PhaseMove()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hitInfo = new RaycastHit();
            bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
            if (hit)
            {
                if (hitInfo.collider.gameObject.tag == "moveabletile")
                {
                    Debug.Log("Move to " + hitInfo.point.x + " " + hitInfo.point.y + " " + hitInfo.point.z);
                    Coordinate targetCoord = Coordinate.fromVecter3(hitInfo.collider.gameObject.transform.position);
                    // get field object
                    selectedCharacter.MoveTo(targetCoord);
                    field.DoEventAtTile(selectedCharacter, targetCoord, "moveon");
                    phase = "pickability"; // go to neext phase
                }
                else
                {
                    phase = "standby";
                }
                field.ClearTile();
            }
        }
    }

    void PhasePickAbility()
    {
        Debug.Log("Pick ability");
        // i choose you attackkk
        //selectedAbility = (Ability)selectedCharacter.abilityList[0];
        selectedAbility = new BananaSplit();
        // when picked
        field.ApplyTargetable(selectedCharacter, selectedAbility);
        phase = "targeting";
    }

    void PhaseConfirmTarget()
    {
        Character[] charactersInAoE = field.GetCharactersInAoE(selectedCharacter.currentCoord, tempAoECoord, selectedAbility);
        if (selectedAbility.target >=0 && charactersInAoE.Length<=0)
        {
            phase = "targeting";
            return;
        }
        foreach (GameObject tmp in GameObject.FindGameObjectsWithTag("targetablecharacter"))
        {
            tmp.tag = "character";
        }
        Debug.Log("Confirm");
        field.ClearTile();
        phase = "action";
        selectedCharacter.CastAbility(selectedAbility, tempAoECoord, charactersInAoE ,field);
         // go to neext phase
    }

    void PhaseTargetting()
    {
        RaycastHit hitInfo = new RaycastHit();
        bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
        if (hit)
        {
            Coordinate targetCoord = Coordinate.fromVecter3(hitInfo.collider.gameObject.transform.position);
            if (targetCoord != tempAoECoord || GameObject.FindGameObjectsWithTag("aoetile").Length<=0)
            {
                tempAoECoord = targetCoord;
                string hitTag = hitInfo.collider.gameObject.tag;
                if (hitTag == "targetabletile" || hitTag == "targetablecharacter" || hitTag == "aoetile")
                {
                    field.ClearTile("aoetile");
                    field.ApplyAoE(selectedCharacter.currentCoord,targetCoord, selectedAbility);
                }
                else
                {
                    field.ClearTile("aoetile");
                }
            }

        }

        // to do action on click 
        if (Input.GetMouseButtonDown(0))
        {
            if (hit)
            {
                Coordinate targetCoord = Coordinate.fromVecter3(hitInfo.collider.gameObject.transform.position);
                tempAoECoord = targetCoord; string hitTag = hitInfo.collider.gameObject.tag;
                if (hitTag == "targetabletile" || hitTag == "targetablecharacter" || hitTag == "aoetile")
                {
                    foreach (GameObject tmp in GameObject.FindGameObjectsWithTag("targetablecharacter"))
                    {
                        tmp.tag = "character";
                    }
                    field.ClearTile("aoetile");
                    phase = "confirmtarget";
                }
                else
                {   // repick ability
                    field.ClearTile();
                    phase = "pickability";
                }

            }
        }
    }
}
