﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuHandler : MonoBehaviour {
	private Text textComp;
	private Color defaultColor;

	void Start()
	{
		this.textComp = gameObject.GetComponent<Text>();
		this.defaultColor = this.textComp.color;
	}

	public void MouseHover()
	{
		this.textComp.color = Color.blue;
	}

	public void MouseExit()
	{
		this.textComp.color = this.defaultColor;
	}

	public void Clicked()
	{
		Debug.Log("Ability menu clicked.");
	}
}
