﻿using UnityEngine;
using System.Collections;

public class Coordinate
{
    public int X { get; set; }
    public int Y { get; set; }
    private static float anchorY = -8f;
    private static float scale = 2.5f;
    private static float anchorX = -11.5f;
    private static float anchorZ = -51.5f;
    public Coordinate()
    {
        this.X = 0;
        this.Y = 0;
    }
    public Coordinate(int x, int y)
    {
        this.X = x;
        this.Y = y;
    }

    public static Coordinate operator +(Coordinate coord1, Coordinate coord2)
    {
        Coordinate result = new Coordinate(coord1.X + coord2.X, coord1.Y + coord2.Y);
        return result;
    }
    public static Coordinate operator -(Coordinate coord1, Coordinate coord2)
    {
        Coordinate result = new Coordinate(coord1.X - coord2.X, coord1.Y - coord2.Y);
        return result;
    }
    public override bool Equals(object obj)
    {
        return this.X == ((Coordinate)obj).X && this.Y == ((Coordinate)obj).Y;
    }
    public static bool operator ==(Coordinate coord1, Coordinate coord2)
    {
        return coord1.Equals(coord2);
    }
    public static bool operator !=(Coordinate coord1, Coordinate coord2)
    {
        return !coord1.Equals(coord2);
    }
    public static Coordinate fromVecter3(Vector3 rawCoordphysicalCoord)
    {
        // calculate coord
        int x = (int)((rawCoordphysicalCoord.x - anchorX) / scale);
        int y = (int)((rawCoordphysicalCoord.z - anchorZ) / scale);
        return new Coordinate(x, y);

    }
    public Vector3 toVecter3()
    {
        // calculate coord
        Vector3 rawCoord = new Vector3(anchorX + (X * scale), anchorY, anchorZ + (Y * scale));
        return rawCoord;

    }
}
