﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Script.CharacterNS;
using Assets.Script.FieldNS;

namespace Assets.Script.AbilityNS
{
    class BananaSplit : Ability
    {
        public BananaSplit() {
            target = -1;
            AoE = new bool[1, 1] { { true } };
            castPattern = new bool[5, 5];
            for (int i = 0; i < castPattern.GetLength(0); i++)
            {
                for (int j = 0; j < castPattern.GetLength(1); j++)
                {
                    castPattern[i, j] = true;
                }
            }
            castPattern[2, 2] = false;
        }
        public override void DoAction(Character caster, Coordinate targetCoord, Character[] cahracterInAoE, Field field)
        {
            FieldObject fieldObject = new BananaTrap(targetCoord);
            field.eventsObjects.Add(fieldObject);
        }
    }
}
