﻿using UnityEngine;
using System.Collections;
using Assets.Script.AbilityNS;
using Assets.Script.CharacterNS;

namespace Assets.Script.AbilityNS
{
    public abstract class Ability
    {
        public int target { get; set; } // team no. 0 allie 1 enemy -1 ground
        public bool[,] AoE { get; set; } // area of efffect 1 affect, 0 not
        public bool[,] castPattern { get; set; }
        public ArrayList typesAmplifiers { get; set; } // condition for extra damage

        public Ability()
        {
            typesAmplifiers = new ArrayList();
        }

        abstract public void DoAction(Character caster, Coordinate targetCoord, Character[] cahracterInAoE, Field field);

    }
}