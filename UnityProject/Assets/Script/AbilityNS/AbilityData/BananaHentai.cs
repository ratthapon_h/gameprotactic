﻿using UnityEngine;
using System.Collections;
using Assets.Script.AbilityNS;
using Assets.Script.CharacterNS;

namespace Assets.Script.AbilityNS
{
    public class BananaHentai : Ability
    {
        public BananaHentai()
        {
            target = 1;
            AoE = new bool[2,3] {{true, true, true},{true, true, true}};
            castPattern = new bool[3, 3] { { false, true, false }, { true, false, true }, { false, true, false } };
        }

        public override void DoAction(Character caster, Coordinate targetCoord, Character[] cahracterInAoE, Field field)
        {
            foreach(Character c in cahracterInAoE){
                c.attribute.currentSP -= 2;
            }
        }
    }
}



