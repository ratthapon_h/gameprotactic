﻿using UnityEngine;
using System.Collections;
using Assets.Script.AbilityNS;
using Assets.Script.CharacterNS;

namespace Assets.Script.AbilityNS
{
    public class BunnyHop : Ability
    {
        public BunnyHop()
        {
            target = -1;
            AoE = new bool[1, 1] { { true } };
            castPattern = new bool[11, 11] { 
                { false , false ,false , false , false , true , false , false , false , false , false },
                { false , false ,false , false , false , true , false , false , false , false , false },
                { false , false ,false , false , false , true , false , false , false , false , false },
                { false , false ,false , false , false , true , false , false , false , false , false },
                { false , false ,false , false , false , true , false , false , false , false , false },
                { true , true ,true , true , true , false , true , true , true , true , true },
                { false , false ,false , false , false , true , false , false , false , false , false },
                { false , false ,false , false , false , true , false , false , false , false , false },
                { false , false ,false , false , false , true , false , false , false , false , false },
                { false , false ,false , false , false , true , false , false , false , false , false },
                { false , false ,false , false , false , true , false , false , false , false , false }
            };
        }

        public override void DoAction(Character caster, Coordinate targetCoord, Character[] cahracterInAoE, Field field)
        {
            caster.MoveTo(targetCoord);
        }
    }
}
