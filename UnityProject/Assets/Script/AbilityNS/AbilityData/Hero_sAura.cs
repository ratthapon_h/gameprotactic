﻿using UnityEngine;
using System.Collections;
using Assets.Script.AbilityNS;
using Assets.Script.CharacterNS;


namespace Assets.Script.AbilityNS
{
    public class Hero_sAura : Ability
    {
        public Hero_sAura()
        {
            target = 1;
            AoE = new bool[3, 3] { { true, true, true }, { true, false, true }, { true, true, true } };
            castPattern = new bool[1, 1] { { true } };
        }

        public override void DoAction(Character caster, Coordinate targetCoord, Character[] cahracterInAoE, Field field)
        {
            foreach (Character c in cahracterInAoE)
            {
                //
            }
        }
    }
}