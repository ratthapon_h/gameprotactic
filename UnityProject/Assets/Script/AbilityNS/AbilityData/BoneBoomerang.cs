﻿using UnityEngine;
using System.Collections;
using Assets.Script.AbilityNS;
using Assets.Script.CharacterNS;

namespace Assets.Script.AbilityNS
{
    public class BoneBoomerang : Ability
    {
        public BoneBoomerang()
        {
            target = 1;
            AoE = new bool[1, 1] { { true } };
            castPattern = new bool[15, 15] { 
                { false , false , false , false ,false , false , false , true , false , false , false , false , false , false , false },
                { false , false , false , false ,false , false , false , true , false , false , false , false , false , false , false }, 
                { false , false , false , false ,false , false , false , true , false , false , false , false , false , false , false }, 
                { false , false , false , false ,false , false , false , true , false , false , false , false , false , false , false },
                { false , false , false , false ,false , false , false , true , false , false , false , false , false , false , false },
                { false , false , false , false ,false , false , false , true , false , false , false , false , false , false , false },
                { false , false , false , false ,false , false , false , true , false , false , false , false , false , false , false },
                { true , true , true , true ,true , true , true , false , true , true , true , true , true , true , true },
                { false , false , false , false ,false , false , false , true , false , false , false , false , false , false , false },
                { false , false , false , false ,false , false , false , true , false , false , false , false , false , false , false },
                { false , false , false , false ,false , false , false , true , false , false , false , false , false , false , false },
                { false , false , false , false ,false , false , false , true , false , false , false , false , false , false , false },
                { false , false , false , false ,false , false , false , true , false , false , false , false , false , false , false },
                { false , false , false , false ,false , false , false , true , false , false , false , false , false , false , false },
                { false , false , false , false ,false , false , false , true , false , false , false , false , false , false , false }
            };
        }

        public override void DoAction(Character caster, Coordinate targetCoord, Character[] cahracterInAoE, Field field)
        {
            foreach (Character c in cahracterInAoE)
            {
                if (c.GetType().ToString() == "BoxingBunny")
                {
                    if (Random.Range(0, 5) > 0)
                        c.attribute.currentHP -= caster.attribute.Atk;
                }
                else
                {
                    c.attribute.currentHP -= caster.attribute.Atk;
                }
            }
        }
    }
}