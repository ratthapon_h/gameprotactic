﻿using UnityEngine;
using System.Collections;
using Assets.Script.AbilityNS;
using Assets.Script.CharacterNS;

namespace Assets.Script.AbilityNS
{
    public class ForMyLord : Ability
    {
        public ForMyLord()
        {
            target = 1;
            AoE = new bool[1, 1] { { true } };
            castPattern = new bool[11, 11] { 
                { false , false ,false , false , false , true , false , false , false , false , false },
                { false , false ,false , false , false , true , false , false , false , false , false },
                { false , false ,false , false , false , true , false , false , false , false , false },
                { false , false ,false , false , false , true , false , false , false , false , false },
                { false , false ,false , false , false , true , false , false , false , false , false },
                { true , true ,true , true , true , false , true , true , true , true , true },
                { false , false ,false , false , false , true , false , false , false , false , false },
                { false , false ,false , false , false , true , false , false , false , false , false },
                { false , false ,false , false , false , true , false , false , false , false , false },
                { false , false ,false , false , false , true , false , false , false , false , false },
                { false , false ,false , false , false , true , false , false , false , false , false }
            };
        }

        public override void DoAction(Character caster, Coordinate targetCoord, Character[] cahracterInAoE, Field field)
        {

            Coordinate tmpCoor = targetCoord;

            foreach(Character c in cahracterInAoE){
                if (c.GetType().ToString() == "BoxingBunny")
                {
                    if (Random.Range(0, 5) > 0)
                        c.attribute.currentHP -= Random.Range(0, 2) == 1 ? caster.attribute.Atk * 6 : caster.attribute.Atk * 3;
                }
                else
                {
                    c.attribute.currentHP -= Random.Range(0, 2) == 1 ? caster.attribute.Atk * 6 : caster.attribute.Atk * 3;
                }
                

                if (caster.currentCoord.X > c.currentCoord.X)
                {
                    tmpCoor.X -= 1;
                }

                if (caster.currentCoord.X < c.currentCoord.X)
                {
                    tmpCoor.X += 1;
                }

                if (caster.currentCoord.Y < c.currentCoord.Y)
                {
                    tmpCoor.Y += 1;
                }

                if (caster.currentCoord.Y > c.currentCoord.Y)
                {
                    tmpCoor.Y -= 1;
                }
            }


            caster.MoveTo(tmpCoor);
            caster.delayTurn += 1;
        }
    }
}