﻿using UnityEngine;
using System.Collections;
using Assets.Script.AbilityNS;
using Assets.Script.CharacterNS;

namespace Assets.Script.AbilityNS
{
    public class Ppp : Ability
    {
        public Ppp()
        {
            target = 1;
            AoE = new bool[1, 1] { { true } };
            castPattern = new bool[3, 3] { { true, true, true }, { true, false, true }, { true, true, true } };
        }

        public override void DoAction(Character caster, Coordinate targetCoord, Character[] cahracterInAoE, Field field)
        {
            foreach (Character c in cahracterInAoE)
            {
                if (c.GetType().ToString() == "BoxingBunny")
                {
                    if (Random.Range(0, 5) > 0)
                    {
                        c.attribute.currentHP -= caster.attribute.Atk + (int)(caster.attribute.Atk * 1.5f);
                        c.delayTurn = 1;
                    }
                }
                else
                {
                    c.attribute.currentHP -= caster.attribute.Atk + (int)(caster.attribute.Atk * 1.5f);
                    c.delayTurn = 1;
                }
            }
        }
    }
}