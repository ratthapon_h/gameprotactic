﻿using UnityEngine;
using System.Collections;
using Assets.Script.AbilityNS;
using Assets.Script.CharacterNS;

namespace Assets.Script.AbilityNS
{
    public class Frightening : Ability
    {
        public Frightening()
        {
            target = 1;
            AoE = new bool[1, 1] { { true } };
            castPattern = new bool[5, 5] { 
            { true, true, true, true, true },
            { true, true, true, true, true },
            { true, true, false, true, true },
            { true, true, true, true, true },
            { true, true, true, true, true } 
            };
        }

        public override void DoAction(Character caster, Coordinate targetCoord, Character[] cahracterInAoE, Field field)
        {
            foreach (Character c in cahracterInAoE)
            {
                //if(c.ToString() == "BoxingBunny"){

                //}
                Coordinate tmpCoor = c.currentCoord;

                if(caster.currentCoord.X > c.currentCoord.X){
                    tmpCoor.X -= 3;
                }

                if (caster.currentCoord.X < c.currentCoord.X)
                {
                    tmpCoor.X += 3;
                }

                if (caster.currentCoord.Y < c.currentCoord.Y)
                {
                    tmpCoor.Y += 3;
                }

                if (caster.currentCoord.Y > c.currentCoord.Y)
                {
                    tmpCoor.Y -= 3;
                }

                c.MoveTo(tmpCoor);
            }
        }
    }
}
