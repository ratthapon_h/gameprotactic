﻿using UnityEngine;
using System.Collections;
using Assets.Script.AbilityNS;
using Assets.Script.CharacterNS;


namespace Assets.Script.AbilityNS
{
    class Hero_sPoJ : Ability
    {
        public Hero_sPoJ()
        {
            target = 1;
            AoE = new bool[3, 3] { { true, true, true }, { true, true , true }, { true, true, true } };
            castPattern = new bool[3, 3] { { false, true, false }, { true, false, true }, { false, true, false } };
        }

        public override void DoAction(Character caster, Coordinate targetCoord, Character[] cahracterInAoE, Field field)
        {
            foreach(Character c in cahracterInAoE){

                    if (c.GetType().ToString() == "BoxingBunny")
                    {
                        if (Random.Range(0, 5) > 0)
                            c.attribute.currentHP -= c.currentCoord == targetCoord ? caster.attribute.Atk * 2 : caster.attribute.Atk;
                    }
                    else
                    {
                        c.attribute.currentHP -= c.currentCoord == targetCoord ? caster.attribute.Atk * 2 : caster.attribute.Atk;
                    }               
            }
        }
    }
}