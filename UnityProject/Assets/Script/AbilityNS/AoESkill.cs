﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Assets.Script.CharacterNS;

namespace Assets.Script.AbilityNS
{
    class AoESkill : Ability
    {
        public AoESkill() {
            target = 1;
            AoE = new bool[3, 3];
            castPattern = new bool[6, 6];
            for (int i = 0; i < AoE.GetLength(0); i++) {
                for (int j = 0; j < AoE.GetLength(1); j++)
                {
                    AoE[i, j] = j>0;
                }
            }
            for (int i = 0; i < castPattern.GetLength(0); i++)
            {
                for (int j = 0; j < castPattern.GetLength(1); j++)
                {
                    castPattern[i, j] = true;
                }
            }
        }
        public override void DoAction(Character caster, Coordinate targetCoord, Character[] characterInAoE, Field field)
        {
            foreach(Character character in characterInAoE){
                character.gameObject.renderer.material = Resources.Load("Damaged", typeof(Material)) as Material;
            }
            Debug.Log("Attackkkkkkkkkkk !!!!!!");
        }
    }
}
