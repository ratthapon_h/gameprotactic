﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Script.CharacterNS;
using UnityEngine;

namespace Assets.Script.FieldNS
{
    abstract public class FieldObject
    {
        public bool isMoveTrigger;
        public bool isConditionTrigger;
        public bool isAttackedTrigger;
        public int lifeTime { get; set; }
        public Coordinate coord { get; set; }
        public List<Status> currentCondition;
        abstract public void ApplyEffectBy(Character character);
    }
}
