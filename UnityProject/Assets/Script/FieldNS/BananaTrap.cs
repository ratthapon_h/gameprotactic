﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Script.CharacterNS;
using UnityEngine;

namespace Assets.Script.FieldNS
{
    class BananaTrap : FieldObject
    {
        private GameObject gameObject { get; set; }
        public BananaTrap(Coordinate settingCoord)
        {
            isAttackedTrigger = false;
            isConditionTrigger = false;
            isMoveTrigger = true;
            this.coord = settingCoord;
            gameObject = GameObject.Instantiate(Resources.Load("BananaTrap")) as GameObject;
            gameObject.transform.position = this.coord.toVecter3();
            lifeTime = -1;
        }
        public override  void ApplyEffectBy(Character character)
        {
            Debug.Log("Banana Activated");
            lifeTime = 0;
            character.delayTurn += 1;
            GameObject.Destroy(gameObject);
        }
    }
}
