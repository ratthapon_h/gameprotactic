﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Script.CharacterNS;
using Assets.Script;
using UnityEngine;

    public class Formation
    {
        public List<Character> characters { get; set; }

        public Formation() {
            characters = new List<Character>();
            Character character1 = new Character(0,5,new Coordinate(4,5));
            Character character2 = new Character(0, 9, new Coordinate(4, 6));
            Character character3 = new Character(0, 7, new Coordinate(4, 7));
            Character character4 = new Character(1, 7, new Coordinate(4, 8));
            Character character5 = new Character(1, 7, new Coordinate(4, 9));
            character1.name = "1";
            character2.name = "2";
            character3.name = "3";
            character4.name = "4";
            character5.name = "5";
            characters.Add(character1);
            characters.Add(character2);
            characters.Add(character3);
            characters.Add(character4);
            characters.Add(character5);
        }
    }
